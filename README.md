# libBulletML

This is a C++ library to handle BulletML, the Bullet Markup Language. It's used
by Kenta Cho's games.

This is a modification of the original library available
[here](http://shinh.skr.jp/libbulletml/index_en.html).

---

libBulletML is BSD (c) shinichiro.h

Modified version of TinyXML is zlib (c) Lee Thomason

Other modifications are (c) Christian Buschau
